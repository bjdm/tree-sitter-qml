// QML language grammar
// ====================
// Reference: http://code.qt.io/cgit/qt/qtdeclarative.git/tree/src/qml/parser/qqmljs.g?h=5.12

module.exports = grammar(require('tree-sitter-javascript/grammar'), {
    name: 'qml',

    conflicts: ($, previous) => previous.concat([
        [$.class, $.class_declaration],
        [$.class_declaration],
        [$.qualified_identifier, $.primary_expression],
    ]),

    inline: ($, previous) => previous.concat([$.property_value]),

    rules: {
        program: $ => seq(
            optional(repeat(choice(
                $.pragma_statement,
                $.import_statement,
                $.relative_import_statement,
            ))),
            $.object_declaration
        ),

        //     //
        //     // Header items
        //     //

        pragma_statement: $ => seq(
            'pragma',
            $.identifier,
            $._semicolon
        ),

        import_statement: ($, previous) => seq(
            'import',
            $.qualified_identifier,
            optional($.number),
            optional($.named_import),
            $._semicolon
        ),

        relative_import_statement: ($, previous) => seq(
            'import',
            $.string,
            optional($.named_import),
        ),

        named_import: $ => seq(
            'as', alias($.identifier, $.type_identifier)
        ),

        //     //
        //     // QML Object
        //     //

        object_declaration: $ => seq(
            $.qualified_identifier, $.object_block
        ),

        object_block: $ => seq(
            '{',
            repeat(choice(
                $.object_declaration,
                $.signal_declaration,
                $.property_declaration,
                $.enum,
                $.property_set,
                $.behaviour_on,
                $.function
            )),
            '}'
        ),

        property_set: $ => seq(
            $.qualified_identifier, field('value', $.property_value), $._semicolon
        ),

        behaviour_on: $ => seq(
            $.qualified_identifier, 'on', $.qualified_identifier, $.object_block
        ),

        //     //
        //     // QML Signal
        //     //

        signal_declaration: $ => seq(
            'signal', $.identifier, optional($.signal_parameters), $._semicolon
        ),

        signal_parameters: $ => seq(
            '(', commaSep(seq($.property_type, $.identifier)), ')'
        ),

        //     //
        //     // QML Property
        //     //

        property_declaration: $ => seq(
            choice(
                $.normal_property,
                $.default_property,
                $.readonly_property,
                $.required_property
            ),
            $._semicolon
        ),

        normal_property: $ => seq($.property_declarator, optional(field('value', $.property_value))),
        default_property: $ => seq('default', $.property_declarator),
        readonly_property: $ => seq('readonly', $.property_declarator, field('value', $.property_value)),
        required_property: $ => seq('required', $.property_declarator),

        property_declarator: $ => seq(
            'property',
            choice(
                $.property_type,
                seq($._type_identifier, '<', $.property_type, '>')
            ),
            alias($.identifier, $.property_identifier)
        ),

        property_type: $ => choice(
            'var',
            $._reserved_identifier,
            $._type_identifier,
            seq($.property_type, '.', $._type_identifier)
        ),

        property_value: $ => seq(
            ':',
            choice(
                $.expression,
                $.script_statement,
                $.object_declaration,
                seq('[', commaSep1($.object_declaration), ']'),
            )
        ),

        //     //
        //     // QML Enum
        //     //

        enum: $ => seq(
            'enum',
            $._type_identifier,
            '{',
            commaSep1($.enum_member),
            '}'
        ),

        enum_member: $ => seq(
            $.identifier,
            optional(seq('=', $.number))
        ),

        //     //
        //     // Statements
        //     //

        script_statement: $ => choice(
            $.statement_block,
            $.if_statement,
            $.with_statement,
            $.switch_statement,
            $.try_statement
        ),

        //     //
        //     // Expressions
        //     //

        qualified_identifier: $ => choice(
            $.identifier,
            pointSep($.identifier),
        ),

        //     //
        //     // Identifiers
        //     //

        _type_identifier: $ => alias($.identifier, $.type_identifier),

        //     _reserved_identifier: ($, previous) => choice(
        //         'enum',
        //         previous
        //     )

    }
});

function pointSep(rule) {
    return seq(rule, repeat(seq('.', rule)));
}

function commaSep1(rule) {
    return seq(rule, repeat(seq(',', rule)));
}

function commaSep(rule) {
    return optional(commaSep1(rule));
}
